package com.tsadigov.dockertest.controller;

import com.tsadigov.dockertest.domain.Student;
import com.tsadigov.dockertest.dto.StudentDTO;
import com.tsadigov.dockertest.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/hello")
    public ResponseEntity<Object> hello(){
        return ResponseEntity.ok("hello");
    }

    @GetMapping
    public ResponseEntity<List<Student>> getAllStudents(){
        return ResponseEntity.ok()
                .body(studentService.getAll());
    }

    @GetMapping("/paginated")
    public ResponseEntity<List<Student>>getStudentsPaginated(@RequestParam int page, @RequestParam int pageSize){
        return ResponseEntity.ok()
                .body(studentService.getAllPaginated(page, pageSize));
    }

    @PostMapping
    public ResponseEntity<String> createStudent(@RequestBody StudentDTO studentDTO){
        studentService.create(studentDTO);
        return ResponseEntity.ok()
                .body("Student created");
    }

}
