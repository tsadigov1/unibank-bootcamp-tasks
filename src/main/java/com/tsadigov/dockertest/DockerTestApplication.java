package com.tsadigov.dockertest;

import com.tsadigov.dockertest.domain.Book;
import com.tsadigov.dockertest.domain.Student;
import com.tsadigov.dockertest.repo.BookRepo;
import com.tsadigov.dockertest.repo.StudentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class DockerTestApplication implements CommandLineRunner {

    private final StudentRepo studentRepo;
    private final BookRepo bookRepo;

    public static void main(String[] args) {
        SpringApplication.run(DockerTestApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

//        Book book = Book.builder()
//                .bookName("White Fang")
//                .build();
//
//        Student student = Student.builder()
//                .name("Brown")
//                .surname("Yellow")
//                .age(12L)
//                .email("brown@gmail.com")
//                .bookList(List.of(book))
//                .build();
//
//        studentRepo.save(student);

    }
}
