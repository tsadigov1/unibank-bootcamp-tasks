package com.tsadigov.dockertest.repo;

import com.tsadigov.dockertest.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepo extends JpaRepository<Student, Long> {
}
