package com.tsadigov.dockertest.service;

import com.speedment.jpastreamer.application.JPAStreamer;
import com.tsadigov.dockertest.domain.Student;
import com.tsadigov.dockertest.dto.StudentDTO;
import com.tsadigov.dockertest.repo.StudentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{

    private final StudentRepo studentRepo;
    private final JPAStreamer jpaStreamer;


    @Override
    public List<Student> getAll() {
        return jpaStreamer.stream(Student.class)
                .sorted()
                .limit(3)
                .collect(Collectors.toList());
    }

    @Override
    public List<Student> getAllPaginated(int page, int pageSize) {

        return jpaStreamer.stream(Student.class)
                .skip((page-1)*pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    @Override
    public void create(StudentDTO studentDTO) {
        Student student = Student.builder()
                .name(studentDTO.getName())
                .surname(studentDTO.getSurname())
                .email(studentDTO.getEmail())
                .age(studentDTO.getAge())
                .build();

        studentRepo.save(student);
    }

}
