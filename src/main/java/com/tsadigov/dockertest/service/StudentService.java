package com.tsadigov.dockertest.service;

import com.tsadigov.dockertest.domain.Student;
import com.tsadigov.dockertest.dto.StudentDTO;

import java.util.List;

public interface StudentService {

    List<Student> getAll();
    List<Student> getAllPaginated(int page, int pageSize);
    void create(StudentDTO studentDTO);

}
